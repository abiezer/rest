package rest.vo;

import java.util.ArrayList;

public class VOUsuario {

	private String usuario;
	private String password;
	private boolean userValido;
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isUserValido() {
		return userValido;
	}
	
	public void setUserValido(boolean b) {
		this.userValido = b;
	}
	
	
	public ArrayList<VOUsuario> get(){
		ArrayList<VOUsuario> usuarios = new ArrayList<VOUsuario>();
		VOUsuario unUsuario = new VOUsuario();
		unUsuario.setUsuario("abiezer");
		unUsuario.setPassword("123");
		usuarios.add(unUsuario);
		
		VOUsuario dosUsuario = new VOUsuario();
		dosUsuario.setUsuario("abiezer2");
		dosUsuario.setPassword("123");
		usuarios.add(dosUsuario);
		
		VOUsuario tresUsuario = new VOUsuario();
		tresUsuario.setUsuario("abiezer3");
		tresUsuario.setPassword("123");
		usuarios.add(tresUsuario);
		
		return usuarios;
	}
	
	
	public VOUsuario edit(VOUsuario vo) {
		VOUsuario tresUsuario = new VOUsuario();
		vo.setUserValido(true);
		if(vo.getUsuario().equals("abiezer")) {
			tresUsuario.setUsuario("abiezer3");
			tresUsuario.setPassword("123");	
		}else {
			tresUsuario.setUsuario("");
			tresUsuario.setPassword("");
		}
		
		return tresUsuario;
	}

}

