package rest.service;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;

import rest.vo.VOUsuario;



@Path("/PrestamosApi")
public class ServiceLoginJR {

	@POST
	@Path("/validarUsuario")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public VOUsuario validaUsuario(VOUsuario vo) {
		vo.setUserValido(false);
		if(vo.getUsuario().equals("user") && vo.getPassword().equals("passwd")) {
			vo.setUserValido(true);
		}
		return vo;
	}
	
	@GET
	@Path("/obtener")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ArrayList<VOUsuario> get(){
		VOUsuario unVOUsuario = new VOUsuario();
		ArrayList<VOUsuario> usuarios = unVOUsuario.get();
		return usuarios;
	}
	
	@PUT
	@Path("/editar")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public VOUsuario update(VOUsuario vo){
		
		VOUsuario unVOUsuario = new VOUsuario();
		VOUsuario usuario = unVOUsuario.edit(vo);
		return usuario;

	}
	
}
